//
//  BackbaseCXP.h
//  BackbaseCXP
//
//  Created by Backbase R&D B.V. on 19/02/15.
//

#import <UIKit/UIKit.h>

//! Project version number for BackbaseCXP.
FOUNDATION_EXPORT double BackbaseCXPVersionNumber;

//! Project version string for BackbaseCXP.
FOUNDATION_EXPORT const unsigned char BackbaseCXPVersionString[];

// exposed interfaces.

// global public constants
#import <BackbaseCXP/BBConstants.h>

// configuration package
#import <BackbaseCXP/BBDevelopmentConfiguration.h>
#import <BackbaseCXP/BBPortalConfiguration.h>
#import <BackbaseCXP/BBSSLPinningConfiguration.h>
#import <BackbaseCXP/BBSecurityConfiguration.h>
#import <BackbaseCXP/BBTemplateConfiguration.h>
#import <BackbaseCXP/BBConfiguration.h>

// rendering package
#import <BackbaseCXP/Renderable.h>
#import <BackbaseCXP/Renderer.h>
#import <BackbaseCXP/WebRenderer.h>
#import <BackbaseCXP/RendererDelegate.h>
#import <BackbaseCXP/BBRendererFactory.h>
#import <BackbaseCXP/NativeView.h>
#import <BackbaseCXP/NativeContract.h>
#import <BackbaseCXP/NativeRenderer.h>
#import <BackbaseCXP/PageRenderer.h>

// model package
#import <BackbaseCXP/SiteMapItemChild.h>
#import <BackbaseCXP/Model.h>
#import <BackbaseCXP/ModelDelegate.h>
#import <BackbaseCXP/StatusCheckerDelegate.h>

// plugins package
#import <BackbaseCXP/Plugin.h>
#import <BackbaseCXP/SyncedPreferences.h>
#import <BackbaseCXP/BBStorage.h>
#import <BackbaseCXP/StorageComponent.h>
#import <BackbaseCXP/AbstractStorageComponent.h>
#import <BackbaseCXP/PersistentStorage.h>
#import <BackbaseCXP/PersistentStorageComponent.h>
#import <BackbaseCXP/SimpleStorage.h>
#import <BackbaseCXP/SimpleStorageComponent.h>
#import <BackbaseCXP/InMemoryStorage.h>
#import <BackbaseCXP/InMemoryStorageComponent.h>

// security package
#import <BackbaseCXP/SecurityViolationDelegate.h>
#import <BackbaseCXP/LoginDelegate.h>
#import <BackbaseCXP/SessionDelegate.h>
#import <BackbaseCXP/SecurityCertificateValidator.h>

// content package
#import <BackbaseCXP/BBContentItem.h>

// targeting package
#import <BackbaseCXP/BBTargetingCallback.h>

// main module
#import <BackbaseCXP/Facade.h>
