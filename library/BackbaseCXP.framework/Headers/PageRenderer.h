//
//  PageRenderer.h
//  BackbaseCXP
//
//  Created by Ignacio Calderon on 13/03/17.
//  Copyright © 2017 Backbase R&D B.V. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BackbaseCXP/BackbaseCXP.h>

@interface PageRenderer : NativeRenderer

@end
