//
//  InMemoryStorage.h
//  BackbaseCXP
//
//  Created by Backbase R&D B.V. on 03/10/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <BackbaseCXP/BackbaseCXP.h>

@interface InMemoryStorage : BBStorage <BBStorageSpec>

@end
