//
//  PersistentStorage.h
//  BackbaseCXP
//
//  Created by Backbase R&D B.V. on 18/05/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <BackbaseCXP/BackbaseCXP.h>

@protocol PersistentStorageSpec <Plugin>
/**
 * Convenience method to retrieve the last known CSRF token.
 * @param callbackId The callbackId that this invocation refers to.
 */
- (void)getCsrfToken:(NSString*)callbackId;
@end

@interface PersistentStorage : BBStorage <PersistentStorageSpec, BBStorageSpec>

@end
