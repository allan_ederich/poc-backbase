//
//  Facade.h
//  BackbaseCXP
//
//  Created by Backbase R&D B.V. on 17/06/15.
//

#import <BackbaseCXP/Backbase+Core.h>
#import <BackbaseCXP/Backbase+PubSub.h>
#import <BackbaseCXP/Backbase+NavigationFlowInformer.h>
#import <BackbaseCXP/Backbase+Logging.h>
#import <BackbaseCXP/Backbase+Model.h>
#import <BackbaseCXP/Backbase+Plugin.h>
#import <BackbaseCXP/Backbase+Preload.h>
#import <BackbaseCXP/Backbase+Rendering.h>
#import <BackbaseCXP/Backbase+Session.h>
#import <BackbaseCXP/Backbase+Performance.h>
#import <BackbaseCXP/Backbase+Security.h>
#import <BackbaseCXP/Backbase+Targeting.h>
#import <BackbaseCXP/Backbase+Content.h>
#import <BackbaseCXP/Backbase+Localization.h>