//
//  BBConstants.h
//  BackbaseCXP
//
//  Created by Backbase R&D B.V. on 20/05/15.
//

#ifndef PUBLIC_CONSTANTS
#define PUBLIC_CONSTANTS

#define CXP_VERSION @"2.18.0"

#pragma mark - Logging

/// Defines logLevels for logging the SDK activity.
typedef NS_ENUM(NSUInteger, BBLogLevel) {
    /// Suppress all internal logs
    BBLogLevelNone = 0,
    /// Only display internal error messages
    BBLogLevelError,
    /// Only display internal error and warnings messages
    BBLogLevelWarn,
    /// Only display internal information, warning and errors messages
    BBLogLevelInfo,
    /// Only display internal debug, information, warning and errors messages
    BBLogLevelDebug,
    /// Logs everything, this is the default value.
    BBLogLevelEverything
};

DEPRECATED_MSG_ATTRIBUTE("Use BBLogLevel enum instead")
typedef NS_ENUM(NSUInteger, CXPLogLevel) {
    /// Suppress all internal logs
    CXPLogLevelNone = 0,
    /// Only display internal error messages
    CXPLogLevelError,
    /// Only display internal error and warnings messages
    CXPLogLevelWarn,
    /// Only display internal information, warning and errors messages
    CXPLogLevelInfo,
    /// Only display internal debug, information, warning and errors messages
    CXPLogLevelDebug,
    /// Logs everything, this is the default value.
    CXPLogLevelEverything
};

#pragma mark - Renderable

/// Possible types of item that can be rendered.
typedef NS_ENUM(NSUInteger, BBItemType) {
    /// Renderable item is link reference
    BBItemTypeLink,
    /// Renderable item is Page
    BBItemTypePage,
    /// Renderable item is a Widget
    BBItemTypeWidget,
    /// Renderable item is a Container / Laout
    BBItemTypeLayout,
    /// Renderable item is an App
    BBItemTypeApp,
    /// Renderable item is a divider
    BBItemTypeDivider
};

DEPRECATED_MSG_ATTRIBUTE("Use BBItemType enum instead")
typedef NS_ENUM(NSUInteger, CXPItemType) {
    /// Renderable item is link reference
    CXPItemTypeLink,
    /// Renderable item is Page
    CXPItemTypePage,
    /// Renderable item is a Widget
    CXPItemTypeWidget,
    /// Renderable item is a Container / Laout
    CXPItemTypeLayout,
    /// Renderable item is an App
    CXPItemTypeApp,
    /// Renderable item is a divider
    CXPItemTypeDivider
};

#pragma mark - Model

/// Preload preference key
extern NSString* const kModelPreferencePreload;

/// Retain preference key
extern NSString* const kModelPreferenceRetain;

#pragma mark - Model Sources

/// Use to load a model from a cached file
extern NSString* const kModelSourceCache;

/// Use to load the model from a server especified in the configurations
extern NSString* const kModelSourceServer;

/// Use to load the model form a local file especified in the configurations
extern NSString* const kModelSourceFile;

#pragma mark - BBNavigationFlowInformer

/// Navigation flow informer self relationship
extern NSString* const kBBNavigationFlowRelationshipSelf;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipSelf constant instead")
extern NSString* const kCXPNavigationFlowRelationshipSelf;

/// Navigation flow informer child relationship
extern NSString* const kBBNavigationFlowRelationshipChild;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipChild constant instead")
extern NSString* const kCXPNavigationFlowRelationshipChild;

/// Navigation flow informer parent relationship
extern NSString* const kBBNavigationFlowRelationshipParent;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipParent constant instead")
extern NSString* const kCXPNavigationFlowRelationshipParent;

/// Navigation flow informer root ancestor relationship
extern NSString* const kBBNavigationFlowRelationshipRootAncestor;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipRootAncestor constant instead")
extern NSString* const kCXPNavigationFlowRelationshipRootAncestor;

/// Navigation flow informer root relationship
extern NSString* const kBBNavigationFlowRelationshipRoot;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipRoot constant instead")
extern NSString* const kCXPNavigationFlowRelationshipRoot;

/// Navigation flow informer sibling relationship
extern NSString* const kBBNavigationFlowRelationshipSibling;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipSibling constant instead")
extern NSString* const kCXPNavigationFlowRelationshipSibling;

/// Navigation flow informer other relationship
extern NSString* const kBBNavigationFlowRelationshipOther;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipOther constant instead")
extern NSString* const kCXPNavigationFlowRelationshipOther;

/// Navigation flow informer external relationship
extern NSString* const kBBNavigationFlowRelationshipExternal;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipExternal constant instead")
extern NSString* const kCXPNavigationFlowRelationshipExternal;

/// Navigation flow informer none relationship
extern NSString* const kBBNavigationFlowRelationshipNone;
DEPRECATED_MSG_ATTRIBUTE("Use kCXPNavigationFlowRelationshipNone constant instead")
extern NSString* const kCXPNavigationFlowRelationshipNone;

#pragma mark - Site map section names

/// Convenient constant name for the Main Navigation site map section
extern NSString* const kSiteMapSectionMainNavigation;

/// Convenient constant name for the Not in Main Navigation site map section
extern NSString* const kSiteMapSectionNotInMainNavigation;
#endif
