//
//  BBTargetingCallback.h
//  BackbaseCXP
//
//  Created by Backbase R&D B.V. on 30/11/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

@protocol Renderable;
typedef void (^BBTargetingSelectCallback)(NSObject<Renderable>* _Nonnull, NSError* _Nullable);
typedef void (^BBTargetingExecuteCallback)(NSString* _Nullable, NSError* _Nullable);
