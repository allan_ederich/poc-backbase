//
//  BBConfiguration.h
//  BackbaseCXP
//
//  Created by Backbase R&D B.V. on 08/02/16.
//  Copyright © 2016 Backbase R&D B.V. All rights reserved.
//

#import <BackbaseCXP/BackbaseCXP.h>

DEPRECATED_MSG_ATTRIBUTE("Use BBConfiguration class instead")
/// Backbase specific configuration
@interface CXPConfiguration : NSObject

/// Portal related configurations
@property (strong, nonatomic) BBPortalConfiguration* portal; //

/// Development related configurations
@property (strong, nonatomic) BBDevelopmentConfiguration* development;

/// Template related configurations
#ifdef __cplusplus
@property (strong, nonatomic, getter=getTemplate, setter=setTemplate:) BBTemplateConfiguration* _template;
#else
@property (strong, nonatomic, getter=getTemplate, setter=setTemplate:) BBTemplateConfiguration* template;
#endif

/// Security related configurations
@property (strong, nonatomic) BBSecurityConfiguration* security;
@end

@interface BBConfiguration : CXPConfiguration
@end
