/**
 * @module ext-frontend-appointments-details-ng
 *
 * @description
 * Default extension for widget-frontend-appointments-details-ng
 *
 * @requires vendor-bb-angular-ng-aria
 */
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';

// uncomment below to include CSS in your extension
// import '../styles/index.css';

export const dependencyKeys = [
  ngAriaModuleKey,
];

export const hooks = {};

export const helpers = {};

export const events = {};
