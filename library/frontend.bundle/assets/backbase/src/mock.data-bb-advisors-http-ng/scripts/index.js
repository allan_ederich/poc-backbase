
import ng from 'vendor-bb-angular';

import advisorsData from './data-bb-advisors-http';

const advisorsDataModuleKey = 'data-bb-advisors-http-ng';

export const advisorsDataKey = 'data-bb-advisors-http-ng:advisorsData';

export default ng
  .module(advisorsDataModuleKey, [])

  .provider(advisorsDataKey, [() => {
    const config = {
      baseUri: '/',
    };

    return {
      setBaseUri: (baseUri) => {
        config.baseUri = baseUri;
      },
      $get: [
        '$q',
        /* into */
       advisorsData(config),
      ],
    };
  }])

  .name;
