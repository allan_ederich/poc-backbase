export default function controller($attrs) {
  function $onInit() {
    console.log('ui-training-appointments-list-ng::$onInit', $attrs);
  }

  return {
    $onInit,
  };
}
