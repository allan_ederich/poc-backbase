const component = {
  bindings: {
    items: '<',
    onSelect: '&',
  },
  controller: 'controller',
  template: `<ul class="list-group">
    <li class="list-group-item" ng-repeat="item in $ctrl.items" ng-click="$ctrl.onSelect({item: item})" ng-class="{ 'urgent': item.urgent}">
      <div class="row row-eq-height">
        <div class="col-xs-5 col-sm-4 col-md-3 col-lg-2 right-border">
      		<div class="text-center col-xs-3 col-sm-4 col-md-4"><span class="fa fa-calendar fa-2x"></span></div>
          	<div class="col-xs-9 col-sm-8 col-md-8 col-lg-8">
              <div class="text-left">
                {{item.date | date:'dd/MM/yyyy'}}
              </div>
              <div class="text-left">
                {{item.date | date:'HH:mm'}}
              </div>
          	</div>
        </div>
        <div class="col-xs-7 col-sm-8 col-md-9 col-lg-10 overflow-hidden-on-xs">
          <h5 ng-bind="item.subject"></h5>
          <div>
            <div class="col-xs-12 col-sm-3">
              <h5>Guests:</h5>
            </div>
            <div class="col-xs-12 col-sm-9">
              <ul>
                <li ng-repeat="guest in item.guests">
                    <h6>
                        <span ng-bind="guest.personalData.fullname"></span> <a href="mailto:{{guest.personalData.email}}" class="hidden-xs">&lt; {{guest.personalData.email}} &gt;</a>
                    </h6>
                  
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </li>
</ul>`,
  // template: require('../templates/main.html'),
};

export default component;
