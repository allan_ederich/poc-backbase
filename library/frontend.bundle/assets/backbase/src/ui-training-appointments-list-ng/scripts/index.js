import angular from 'vendor-bb-angular';

import component from './component';
import controller from './controller';

import '../styles/index.css';

export default angular
  .module('ui-training-appointments-list-ng', [])
  .component('uiTrainingAppointmentsListNg', component)
  .controller('controller', ['$attrs', controller])
  .name;
