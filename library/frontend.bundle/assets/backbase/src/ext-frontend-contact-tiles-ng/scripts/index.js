/**
 * @module ext-frontend-contact-tiles-ng
 *
 * @description
 * Ext-frontend-contact-tiles-ng
 *
 * @requires vendor-bb-angular-ng-aria
 */
import ngAriaModuleKey from 'vendor-bb-angular-ng-aria';

import '../styles/index.css';

import extHooks from './hooks';

// uncomment below to include CSS in your extension
import '../styles/index.css';

import uiBbAvatarKey from 'ui-bb-avatar-ng';
export const dependencyKeys = [
  ngAriaModuleKey,
  uiBbAvatarKey,
];

export const hooks = (context) => {
  return extHooks(context);
};

export const helpers = {};

export const events = {};
