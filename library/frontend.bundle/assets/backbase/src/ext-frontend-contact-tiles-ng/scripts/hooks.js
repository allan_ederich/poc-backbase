const RANDOM_USER_API_ENDPOINT = 'randomUserApiEndpoint';
export default (context) => {
  const randomUserApiEndpoint = context.widget.getStringPreference(RANDOM_USER_API_ENDPOINT);
  return {
    processContacts: (contacts) => {
      return contacts.map((contact, index) => {
        contact.imageAvatar =
          (randomUserApiEndpoint) ? randomUserApiEndpoint.replace('${userIndex}', index + '.jpg') : "nullAvatar.jpg";
          contact.name = 'Tom Doe';
        return contact;
      });
    }
  };
};
