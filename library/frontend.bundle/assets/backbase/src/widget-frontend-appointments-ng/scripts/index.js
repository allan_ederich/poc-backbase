/**
 * @module widget-frontend-appointments-ng
 *
 * @description
 * Appointments
 */
import angular from 'vendor-bb-angular';

import widgetModuleKey, { widgetKey } from 'lib-bb-widget-ng';
import eventBusModuleKey, { eventBusKey } from 'lib-bb-event-bus-ng';


import modelAppointmentsModuleKey, {
    modelAppointmentsKey,
} from 'model-frontend-appointments-ng';


import Controller from './controller';

import extendHooks from 'lib-bb-widget-extension-ng';
import defaultHooks from './default-hooks';

const moduleKey = 'widget-frontend-appointments-ng';
const hooksKey = `${moduleKey}:hooks`;

export default angular
  .module(moduleKey, [
    widgetModuleKey,
    eventBusModuleKey,

    modelAppointmentsModuleKey,

  ])

  .factory(hooksKey, extendHooks(defaultHooks))

  .controller('AppointmentsController', [
    // dependencies to inject
    eventBusKey,
    hooksKey,

    modelAppointmentsKey,

    /* into */
    Controller,
  ])

  .run([eventBusKey, widgetKey, (bus, widget) => {
    bus.publish('cxp.item.loaded', {
      id: widget.getId(),
    });

    // for mobile devices
    gadgets.pubsub.subscribe("hello", function(evt) {
     console.log(evt.data); // will print "text to be delivered"
    });

    setTimeout(function(){
      gadgets.pubsub.publish("sayhello",
         {
           data: "text to be delivered"
         }
      );
    }, 3000);

    }])

  .name;
