import widget from './index';

describe('Appointments', () => {
  it('should export the module name', () => {
    expect(widget).toEqual('widget-frontend-appointments-ng');
  });
});
