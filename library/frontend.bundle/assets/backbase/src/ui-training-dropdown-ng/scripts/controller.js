export default function controller($attrs) {
  function $onInit() {
    const $ctrl = this;
    $ctrl.selected = null;
    $ctrl.select = function (item) {
      $ctrl.selected = item;
      $ctrl.onSelect({item: item});
    };
  }

  return {
    $onInit,
  };
}
