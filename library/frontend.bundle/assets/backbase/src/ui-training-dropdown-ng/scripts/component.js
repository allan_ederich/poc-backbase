const component = {
  bindings: {
    items: '<',
    itemType: '<',
    onSelect: '&',
  },
  controller: 'controller',
  template: `<div class="dropdown">
		<div>{{$ctrl.itemType}}:</div>
		<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown">
			<span ng-if="$ctrl.selected">{{$ctrl.selected.personalData.fullname}} </span> <span ng-if="!$ctrl.selected">Select one {{$ctrl.itemType}} </span><span class="caret"></span>
		</button>
		<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
			<li ng-click="$ctrl.select(null)">
				<a role="menuitem" tabindex="-1" href="#" >Deselect</a>
			</li>
			<li ng-repeat="item in $ctrl.items" ng-click="$ctrl.select(item)">
				<a role="menuitem" tabindex="-1" href="#" ng-bind="item.personalData.fullname"></a>
			</li>
		</ul>
</div>`,
  // template: require('../templates/main.html'),
};

export default component;
