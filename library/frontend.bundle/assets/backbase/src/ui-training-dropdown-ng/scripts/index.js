import angular from 'vendor-bb-angular';
import 'vendor-bootstrap-dropdown';

import component from './component';
import controller from './controller';

import '../styles/index.css';

export default angular
  .module('ui-training-dropdown-ng', [])
  .component('uiTrainingDropdownNg', component)
  .controller('controller', ['$attrs', controller])
  .name;
