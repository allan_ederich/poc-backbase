
import ng from 'vendor-bb-angular';

import customersData from './data-bb-customers-http';

const customersDataModuleKey = 'data-bb-customers-http-ng';

export const customersDataKey = 'data-bb-customers-http-ng:customersData';

export default ng
  .module(customersDataModuleKey, [])

  .provider(customersDataKey, [() => {
    const config = {
      baseUri: '/',
    };

    return {
      setBaseUri: (baseUri) => {
        config.baseUri = baseUri;
      },
      $get: [
        '$q',
        /* into */
       customersData(config),
      ],
    };
  }])

  .name;
