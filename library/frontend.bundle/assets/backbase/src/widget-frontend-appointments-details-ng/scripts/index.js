/**
 * @module widget-frontend-appointments-details-ng
 *
 * @description
 * Appointments details
 */
import angular from 'vendor-bb-angular';
import 'vendor-bootstrap-modal';

import widgetModuleKey, { widgetKey } from 'lib-bb-widget-ng';
import eventBusModuleKey, { eventBusKey } from 'lib-bb-event-bus-ng';



import Controller from './controller';

import extendHooks from 'lib-bb-widget-extension-ng';
import defaultHooks from './default-hooks';

const moduleKey = 'widget-frontend-appointments-details-ng';
const hooksKey = `${moduleKey}:hooks`;

export default angular
  .module(moduleKey, [
    widgetModuleKey,
    eventBusModuleKey,

  ])

  .factory(hooksKey, extendHooks(defaultHooks))

  .controller('AppointmentsDetailsController', [
    // dependencies to inject
    eventBusKey,
    hooksKey,
    '$scope',

    /* into */
    Controller,
  ])

  .run([eventBusKey, widgetKey, (bus, widget) => {
    bus.publish('cxp.item.loaded', {
      id: widget.getId(),
    });
  }])

  .name;
