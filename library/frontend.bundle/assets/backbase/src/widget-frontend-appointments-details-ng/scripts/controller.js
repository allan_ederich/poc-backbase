/**
 * @module widget-frontend-appointments-details-ng
 * @name AppointmentsDetailsController
 *
 * @description
 * Appointments details
 */

export default function AppointmentsDetailsController(bus, hooks, $scope) {
  const $ctrl = this;
  $ctrl.appointment = null;

  /**
   * AngularJS Lifecycle hook used to initialize the controller
   *
   * @name AppointmentsDetailsController#$onInit
   * @returns {void}
   */
  const $onInit = () => {
    bus.subscribe('appointment:showDetails', function (appointment) {
        $scope.$evalAsync( function () {
            $ctrl.appointment = appointment;
        });
    });

  };

  Object.assign($ctrl, {
    $onInit,

  });
}
