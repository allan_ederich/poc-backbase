var mobile = mobile || {};
mobile.widgets = mobile.widgets || {};
mobile.widgets.native = (function(gadgets) {

    var Native = function(widget) {
        var self = this;
        this.widget = widget;

        // Initialize state of the widget
        this.reloadData(widget);

        // Reload the widget's data when a preference has changed
        this.widget.model.addEventListener('PrefModified', function(e) {
            self.reloadData(self.widget);
        }, false, widget);
    };

    Native.prototype.reloadData = function(widget) {

        // Apply label of the native class
        var nativeClass = widget.getPreference('native');
        widget.body.querySelector('.class').innerHTML = nativeClass;
    };

    return {
        init: function(widget) {
            var native = new Native(widget);
        }
    };

})(gadgets);
