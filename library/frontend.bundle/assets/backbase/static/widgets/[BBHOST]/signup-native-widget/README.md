# Signin Native Widget #
The Signin Native Widget is a widget that enables CXP Manager users to define placeholders in pages that will be rendered using [native widgets](https://my.backbase.com/docs/product-documentation/documentation/mobile-sdk/latest/native_rendering.html).

![Example](example.jpg)

## Usage ##
The widget uses a preference named "native" and labeled "Native class" in order to instruct the Mobile SDK upon rendering what native class should be rendered. This preference can be changed by opening the widget's preferences pane.

Refer to the [official documentation](https://my.backbase.com/docs/product-documentation/documentation/mobile-sdk/latest/native_rendering.html) for additional information about native rendering.

## Installation ##
1. ZIP the content of this repository (excluding any repository information such as the .git folder). Make sure that *model.xml* is in the root of the archive;
2. Log in as an admin user on CXP Manager and navigate to the Enterprise Catalog;
3. Import the Universal Native Widget by dragging the ZIP file created during step 1 in the Enterprise Catalog. Enable "Add to portal catalogs" to make it available to all existing portals;
4. Navigate to the homepage of CXP Manager and open the portal representing your mobile app;
5. Open the page where you want the Universal Native Widget to be shown;
6. Drag the Universal Native Widget from the widget collection in the page at the position where you want the Universal Native Widget to be shown.

> NOTE: If the Universal Native Widget cannot be found in the widget collection, make sure the widget is available in the Portal Catalog of the portal.


## Additional notes ##

- The Universal Native Widget has been tested with CXP 5.6.2.

## License ##
MIT License

Copyright (c) 2016 Backbase

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
media
