//
//  NativeWidgetViewController.swift
//  poc
//
//  Created by Ivens Denner on 26/04/17.
//  Copyright © 2017 Allan Ederich. All rights reserved.
//

import UIKit
import BackbaseCXP

class NativeWidgetViewController: UIViewController {
    
    private var page: Renderable!
    private var renderer: Renderer?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let model = Backbase.currentModel()
        
        let siteMapItems = model?.siteMapItemChildren(for: kSiteMapSectionMainNavigation) as! [SiteMapItemChild]
        let pageSiteMap = siteMapItems.filter({ $0.title() == "Nativo" }).first
        self.page = model?.item(byId: pageSiteMap?.itemRef())
        
        self.navigationItem.title = self.page.preference(forKey: "title")
        self.renderItem()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.renderer?.dispatchEvent!("controller.will.appear", payload: nil)
    }

    private func renderItem() {
        do {
            // Create renderer
            self.renderer = try BBRendererFactory.renderer(forItem: self.page)
            // Render page
            try self.renderer?.start(self.view)
            self.applyPreferences(renderable: self.page)
        }
        catch {
            print("Something went wrong")
            print("print: \(error.localizedDescription)")
        }
    }
    
    private func applyPreferences(renderable: Renderable) {
        // Set background image if presented
//        if let background = renderable.preference(forKey: "background") {
//            let imageView = UIImageView(image: UIImage(named: background))
//            imageView.contentMode = .scaleAspectFill
//            imageView.frame = self.view.bounds
//            imageView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
//            imageView.clipsToBounds = true
//            
//            self.view.insertSubview(imageView, at: 0)
//        }
    }

}
