//
//  ViewController.swift
//  poc
//
//  Created by Allan Ederich on 18/04/17.
//  Copyright © 2017 Allan Ederich. All rights reserved.
//

import UIKit
import BackbaseCXP

class ViewController: UIViewController {
    
//    @IBOutlet weak var contentView: UIView!
//    @IBOutlet weak var textField: UITextField!
    
    var renderer: Renderer?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.view.backgroundColor = UIColor.gray
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        loadBackbasePage()
    }
    
    func loadBackbasePage() {
        
        let model = Backbase.currentModel()
        
        let siteMap = model?.siteMapItemChildren(for: kSiteMapSectionMainNavigation)
        
        siteMap?.forEach { obj in
            
            if let siteMapChild = obj as? SiteMapItemChild {
                
                if siteMapChild.title() == "SignInPage" {
                    
                    let renderable = model?.item(byId: siteMapChild.itemRef())
                    
                    do {
                        
                        renderer = try BBRendererFactory.renderer(forItem: renderable)
                        
                        try renderer?.start(self.view)
                        renderer?.reload()
                    } catch {
                        
                        print("ERROR: \(error)")
                    }
                }
                
            }
        }
    }
    
//    @IBAction func didTouchBtn(_ sender: UIButton) {
//        
//        if let cpf = textField.text {
//            
//            Backbase.publishEvent("getCPF", payload: ["data": cpf])
//        }
//    }
}
