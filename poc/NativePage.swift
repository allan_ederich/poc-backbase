//
//  NativePage.swift
//  poc
//
//  Created by Ivens Denner on 27/04/17.
//  Copyright © 2017 Allan Ederich. All rights reserved.
//

import UIKit
import BackbaseCXP

class NativePage: UIView, Renderer {
    
    private let page: Renderable
    private var childRenderer: Renderer?
    private var childRenderers: [Renderer] = []
    
    private let stackView: UIStackView

    required init!(frame: CGRect, item: Renderable!) {
        self.page = item
        self.stackView = UIStackView()
        
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start(_ container: UIView!) throws {
//        // check if the page contains more or less than one child. Reject rendering if requirements are not met.
//        if self.page.itemChildren().count != 1{
//            throw StartError.InvalidArgumentException("Child can only have one child")
//        }
//        
//        // check that the renderer can be created for the item
//        childRenderer = try BBRendererFactory.renderer(forItem: (self.page.itemChildren() as? [Renderable])?[0])
//        
//        // add as a subview of the container and apply constraints to match the parent size.
//        container.addSubview(self)
//        self.createConstraints(container: container)
//        try childRenderer?.start(self)
        self.childRenderers = []
        
        let itemChildren = self.page.itemChildren() as? [Renderable] ?? []
        for itemChild in itemChildren {
            let renderer = try BBRendererFactory.renderer(forItem: itemChild)
            self.childRenderers.append(renderer)
        }
    }
    
    func reload() {
        self.childRenderer?.reload()
    }
    
    func size() -> CGSize {
        return self.bounds.size
    }
    
    func item() -> Renderable! {
        return self.page
    }
    
    func enableScrolling(_ enable: Bool) {
        self.childRenderer?.enableScrolling(enable)
    }
    
    func isScrollingEnabled() -> Bool {
        return self.childRenderer?.isScrollingEnabled() ?? false
    }
    
    func enableBouncing(_ enable: Bool) {
        self.childRenderer?.enableBouncing(enable)
    }
    
    func isBouncingEnabled() -> Bool {
        return self.childRenderer?.isBouncingEnabled() ?? false
    }

}
