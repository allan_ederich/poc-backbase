//
//  ConteudoViewController.swift
//  poc
//
//  Created by Allan Ederich on 18/04/17.
//  Copyright © 2017 Allan Ederich. All rights reserved.
//

import UIKit
import BackbaseCXP

class ConteudoViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIView!
    var renderer: Renderer?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        loadBackbasePage()
    }
    
    func loadBackbasePage() {
        
        let model = Backbase.currentModel()
        
        let siteMap = model?.siteMapItemChildren(for: kSiteMapSectionMainNavigation)
        
        siteMap?.forEach { obj in
            
            if let siteMapChild = obj as? SiteMapItemChild {
                
                if siteMapChild.title() == "cpf" {
                    
                    let renderable = model?.item(byId: siteMapChild.itemRef())
                    
                    do {
                        
                        renderer = try BBRendererFactory.renderer(forItem: renderable)
                        
                        try renderer?.start(contentView)
                    } catch {
                        
                        print("ERROR: \(error)")
                    }
                }
                
            }
        }
    }

}
