//
//  MainViewController.swift
//  poc
//
//  Created by Allan Ederich on 19/04/17.
//  Copyright © 2017 Allan Ederich. All rights reserved.
//

import UIKit
import BackbaseCXP

class MainViewController: UIViewController {

    override func viewDidLoad() {
        
        super.viewDidLoad()
    }
    
    @IBAction func didTouchInitialize(_ sender: UIButton) {
        
        do {
            try Backbase.initialize("assets/backbase/conf/configs-ios.json", forceDecryption: false)
        } catch {
            print("Couldn't load config file")
        }

        self.registerRenderers()
        
        Backbase.startSession("admin", password: "admin", delegate: self)
    }
    
    private func registerRenderers() {
        do {
            try BBRendererFactory.registerRenderer(SignInWidget.self)
            try BBRendererFactory.registerRenderer(SocialWidget.self)
        } catch {
            print("ERROR! Could register renderers")
            print(error)
        }
    }
}

extension MainViewController: ModelDelegate {
    
    func modelDidLoad(_ model: Model!) {
        
        print("Did load model")
    }
    
    func modelDidFailLoadWithError(_ error: Error!) {
        
        print("Couldn't load model")
    }
    
}

extension MainViewController: LoginDelegate {
    
    func loginDidFailWithError(_ error: Error!) {
        
        print(error.localizedDescription)
    }
    
    func loginDidSucceed(withCode responseCode: Int) {
        
        print(responseCode)
        
        Backbase.model(self, order: [kModelSourceServer])
    }
}
