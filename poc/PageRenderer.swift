//
//  PageRenderer.swift
//  SicrediDigital
//
//  Created by Ivens Denner on 02/05/17.
//  Copyright © 2017 Sicredi Digital. All rights reserved.
//

import UIKit
import BackbaseCXP

@objc(PageRenderer)
class PageRenderer: UIView, Renderer {
    
    private var page: Renderable
    private var childRenderers: [Renderer]

    required init(frame: CGRect, item: Renderable!) {
        self.page = item
        self.childRenderers = []
        
        super.init(frame: frame)
        
        self.initialSetup()
    }
    
    required init(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func initialSetup() {
        
        self.translatesAutoresizingMaskIntoConstraints = false
//        self.axis = .vertical
//        self.alignment = .center
//        self.distribution = .fill
    }
    
    func size() -> CGSize {
        return self.bounds.size
    }
    
    func start(_ container: UIView!) throws {
        guard let itemChildren = self.page.itemChildren() as? [Renderable] else {
            return
        }
        
        for itemChild in itemChildren {
            let childRenderer = try BBRendererFactory.renderer(forItem: itemChild)
            self.childRenderers.append(childRenderer)
        }
        
        container.addSubview(self)
        
        self.createConstraints(container: container)
        
        try self.childRenderers.forEach({
//            self.addSubview($0 as! UIView)
            try $0.start(self)
        })
    }
    
    func reload() {
        self.childRenderers.forEach({ $0.reload() })
    }
    
    func item() -> Renderable! {
        return self.page
    }
    
    func enableScrolling(_ enable: Bool) {
        
    }
    
    func isScrollingEnabled() -> Bool {
        return false
    }
    
    func enableBouncing(_ enable: Bool) {
        
    }
    
    func isBouncingEnabled() -> Bool {
        return false
    }
    
    func createConstraints(container: UIView){
        self.topAnchor.constraint(equalTo: container.topAnchor).isActive = true
        self.centerXAnchor.constraint(equalTo: container.centerXAnchor)
        
        container.setNeedsUpdateConstraints()
        container.updateConstraintsIfNeeded()
    }

}
