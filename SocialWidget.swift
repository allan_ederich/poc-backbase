//
//  SocialWidget.swift
//  poc
//
//  Created by Ivens Denner on 03/05/17.
//  Copyright © 2017 Allan Ederich. All rights reserved.
//

import UIKit
import BackbaseCXP

@objc(SocialWidget)
class SocialWidget: UIView, Renderer {
    
    private var widget: Renderable

    required init!(frame: CGRect, item: Renderable!) {
        self.widget = item
        
        super.init(frame: frame)
        
        self.translatesAutoresizingMaskIntoConstraints = false
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start(_ container: UIView!) throws {
        container.addSubview(self)
        Backbase.publishEvent("social-widget-loaded", object: self, payload: ["id" : self.widget.itemId()])
        
        self.backgroundColor = UIColor.red
        
        self.createConstraints(container: container)
        
        self.setNeedsUpdateConstraints()
        self.updateConstraintsIfNeeded()
    }
    
    private func createConstraints(container: UIView) {
        let views = ["self": self]
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[self]|",
                                                                options: [],
                                                                metrics: nil,
                                                                views: views))
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[self]|",
                                                                options: [],
                                                                metrics: nil,
                                                                views: views))
        
        container.heightAnchor.constraint(equalToConstant: 700).isActive = true
    }
    
    func reload() {
        
    }
    
    func size() -> CGSize {
        return CGSize.zero
    }
    
    func item() -> Renderable! {
        return nil
    }
    
    func enableScrolling(_ enable: Bool) {
        
    }
    
    func isScrollingEnabled() -> Bool {
        return true
    }
    
    func enableBouncing(_ enable: Bool) {
        
    }
    
    func isBouncingEnabled() -> Bool {
        return true
    }

}
