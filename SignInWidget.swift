//
//  SignInWidget.swift
//  poc
//
//  Created by Ivens Denner on 02/05/17.
//  Copyright © 2017 Allan Ederich. All rights reserved.
//

import UIKit
import BackbaseCXP

@objc(SignInWidget)
class SignInWidget: UIView, Renderer {
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    private let widget: Renderable
    
    required init!(frame: CGRect, item: Renderable!) {
        self.widget = item
        
        super.init(frame: frame)
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        let view = Bundle.main.loadNibNamed("SignInWidget", owner: self, options: nil)!.first as! UIView
        self.addSubview(view)
        
        let views = ["view": view]
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-8-[view]-8-|",
                                                           options: [],
                                                           metrics: nil,
                                                           views: views))
        self.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-8-[view]-8-|",
                                                           options: [],
                                                           metrics: nil,
                                                           views: views))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func start(_ container: UIView!) throws {
        container.addSubview(self)
        
        Backbase.publishEvent("signin-loaded", object: self, payload: ["id" : self.widget.itemId()])
        
        self.createConstraints(container: container)
        
        self.setNeedsLayout()
        self.layoutIfNeeded()
        
        self.setNeedsUpdateConstraints()
        self.updateConstraintsIfNeeded()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        let view = self.subviews.first
        view?.frame = self.frame
    }
    
    private func createConstraints(container: UIView) {
        let views = ["self": self]
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[self]|",
                                                                options: [],
                                                                metrics: nil,
                                                                views: views))
        container.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[self]|",
                                                                options: [],
                                                                metrics: nil,
                                                                views: views))
        
        container.heightAnchor.constraint(equalTo: self.heightAnchor, multiplier: 1)
    }
    
    func reload() {
        self.layoutIfNeeded()
    }
    
    func size() -> CGSize {
        return self.bounds.size
    }
    
    func item() -> Renderable! {
        return self.widget
    }
    
    func enableScrolling(_ enable: Bool) {
        
    }
    
    func isScrollingEnabled() -> Bool {
        return false
    }
    
    func enableBouncing(_ enable: Bool) {
        
    }
    
    func isBouncingEnabled() -> Bool {
        return false
    }
    
    @IBAction func confirmAction(_ button: UIButton) {
        print("USERNAME: \(loginTextField.text ?? "none")")
        print("PASSWORD: \(passwordTextField.text ?? "none")")
    }

}
